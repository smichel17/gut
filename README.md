Why `gut`?
----

`git` has an amazing architecture... but you shouldn't have to understand it to use the tool. You should be able to *follow your gut.*

`gut` is a wrapper for git, aimed at making git more intuitive, simpler, and less confusing.

Preface
----

This project uses [Readme Driven Development](http://tom.preston-werner.com/2010/08/23/readme-driven-development.html). That means this document describes the final (1.0) state of gut, not its current state.

All contriubtions are welcomed, whether they're feedback, ideas, code, etc. You're invited to take a gander through the issues section of this repository.

So how do I use `gut`?
----

### Getting Started

#### Initialize an empty repository

`gut init` creates a new, empty repository in the current directory.

[[Picture: title text: "gut init". one node w/ arrow pointing to it w/ text, "empty repository"]]

#### Clone an existing repository

- `gut clone $repoURL $path/to/local/folder` will download a remote repository to the specified folder and configure sync commands to use that remote repo by default.
  - If `$path/to/local/folder` is omitted, gut will create/use a folder in the current directory with the same name as the remote repository.

### Commit your changes

A commit is a snapshot of a certain point in history. In order to create a commit, you must first stage all the changes that belong in that commit.

- `gut status` will show you what files you've changed since the last commit.
- `gut add $file` stages the current state of the file. 
    - *note: you'll use the same command to stage the deletion of a file.*
- `gut drop $file` is the inverse of `gut add`. It unstages changes, reverting the staged version to the state of the previous commit.
- `gut commit '$your_commit_message'` commits staged changes.

A commit is uniquely identified by its commit commitID.

- `gut log` will show your commit history.

[[Picture: title text: "history".  4 commits, one's the initial commit w/ same arrow & text. The others have short commit commitIDs pointing to them.]]

### I messed up! What do I do?

`gut reset` is your panic button. It can be used in a couple different ways.

> **DANGER: This command is destructive. Think before you reset.**

- `gut reset $file` will revert the file to its state when it was last staged (it's possible this is the same as the state of the most recent commit). If no changes have been made since that time, no action will be taken.
- `gut reset $file $commitID` will revert the file to its state in the given commit.

OPTIONS:

- `-a` or `--all` will revert all files. When used with a commit commitID, it will also discard more recent commits.

[[picture: title text: gut reset -a $commitID. Later commits have been erased]]

### Develop unrelated features in parallel

Sometimes you want to work on two features, but not worry about how they interact just yet. As if the history were a tree, you can create a *branch* where you can work in parallel. The default, main branch is called `master`.

[[picture: previous picture, now labeled as master, with a `feature` branch added.]] 

- `gut branch` will list all branches.
- `gut checkout $branchName` will change the files in the working directory so you can work on another branch. Everything in your current branch (commited, added, or not) will be automatically saved for your return.
- `gut branch $branchName $commitID` will create and checkout a new branch which diverges at the given commit.  
    - If you omit the commitID, the current branch will be reverted to the previous commit and any changes since then will be moved to the new branch.
- `gut branch -d $branchName` will delete a branch.
- `gut pull $branchName` will attempt to integrate changes from the given branch into the active branch, from the most recent point where they share history.
    - If the changes do not conflict with changes made to the active branch (or if the acive branch contains no changes), this will happen automatically.  If not, you will be required to manually merge conflicts.
- `gut push $branchName` is the equivalent of pulling the active branch from the given branch. However, it will only work if there are no conflicts.
    - If there are conflicts, you should check out the target branch and use `gut pull` instead.

### History

- `gut checkout $commitID` will allow you to jump back to a previous commit without affecting the present state of the repository. You can return to the present by checking out a branch again (ex: `gut checkout master`).
    - History can't be changed, so while you can make and commit changes as normal, NOTHING WILL BE SAVED unless you either create a new branch to save your work on or push your work to a branch.

### Remote repositories

The default remote is where the local repository was cloned from. It can also be set manually (see below).

- `gut remote add $repoName $repoURL` will add a remote repository
- `gut pull $repoName $branchName` will retrieve changes from that branch of the remote repo
    - If $branchName is omitted, gut will use the name of the active branch.
        - note: if there is a branch with the same name as the remote, gut will assume you meant the branch and not the remote. Don't name your remotes the same as your branches!
    - If $repoName is omitted, gut will use the default remote.
    - `gut pull -a $repoName` or `gut pull --all $repoName` will pull all remote branches into local branches with the same name. If a local branch with the same name does not exist, it will create one.
- `gut push` can also be used with remote branches, with all the same syntax. Like with local branches, it will fail if the branches cannot be merged automatically.
    - Since you cannot check out a remote branch, if there are conflicts, the correct process is to first pull from the remote, do the merge manually, commit, then push.
    - `gut sync $repoName` is the equivalent of running `gut pull -a $repoName`, followed by `gut push -a $repoName`, automating this process.
        - Like the commands it runs, you can omit $repoName and use the default repo.
        - If the pull requires manual merging, this command will stop before pushing and you must run it again after you manually merge.
        - `gut sync --set-default $repoName` will also set the default remote. It will only do this if the sync completes.

### Complex Behavior

Gut's goal is to be simple. But is a wrapper for git, so if you need to do a complex operation, you can use git.
